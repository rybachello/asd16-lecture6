FactoryGirl.define do
  factory :user do
    full_name "MyString"
    username "MyString"
    email "MyString"
    password "MyString"
    password_confirmation "MyString"
    confirmed_at "2016-10-04 10:58:09"
    role "MyString"
  end
end
